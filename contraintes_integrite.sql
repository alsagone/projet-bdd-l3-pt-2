-- Hakim ABDOUROIHAMANE
-- Hicheme BEN GAIED

-------------- CONTRAINTES D'INTEGRITE --------------
-- Question 1: Un utilisateur aura un maximum de 300 vidéos en favoris.

CREATE TRIGGER TRIGG_FAVORIS
BEFORE INSERT ON favoris
FOR EACH ROW
BEGIN
DECLARE nb_favoris INTEGER DEFAULT 0;

	SELECT favoris.login, 
    COUNT(favoris.idVideo) into nb_favoris
    FROM favoris
    WHERE(favoris.login = new.login)
    GROUP BY favoris.login;
    
    IF (nb_favoris = 300)
    THEN
		RAISE_APPLICATION_ERROR(-20365, "Impossible d'ajouter la vidéo " | new.idVideo | ": l'user " | new.login | " a atteint la limite de favoris (300).");
	END IF;
END;

-- Question 2: Si une diffusion d’une émission est ajoutée, les dates de disponibilités seront mises à jour.
-- La nouvelle date de fin de disponibilité sera la date de la dernière diffusion plus 14 jours.

CREATE TRIGGER mise_a_jour_disponibilites
AFTER INSERT OR UPDATE on dates_diffusion
FOR EACH ROW
BEGIN
    UPDATE video 
        SET date  = :new.date + 14
        WHERE idVideo = :new.idVideo;     
END;

-- Question 3: La suppression d'une vidéo entraînera son archivage dans une table des vidéos qui ne sont plus accessibles
CREATE TRIGGER TRIG_ARCHIVAGE
BEFORE DELETE on video
FOR EACH ROW
BEGIN
    INSERT INTO archives (idVideo) 
    VALUES :old.idVideo;
END;

-- Question 4: Un utilisateur ne peut pas regarder plus de 3 vidéos par minutes
CREATE TRIGGER TRIG_VISIONNAGE
BEFORE INSERT on historique
FOR EACH ROW
BEGIN
    DECLARE nb_visionnages NUMBER; 

    SELECT historique.login, COUNT(historique.date_visionnage) into nb_visionnages
    FROM historique
    WHERE ((CURRENT_DATE - date_visionnage) * 24 * 60 * 60 <= 60);     -- différence des deux dates en secondes

    IF (nb_visionnages = 3)
    THEN
        RAISE_APPLICATION_ERROR(-20185, historique.login || " a déjà regardé 3 vidéos sur la dernière minute");
    END IF;

END;