-- Hakim ABDOUROIHAMANE
-- Hicheme BEN GAIED

-------------- PROCEDURES ET FONCTIONS PL/SQL --------------

-- Question 1: Convertit au format JSON les informations d'une vidéo
CREATE PROCEDURE `infosJSON` (idVideo VARCHAR(255))
BEGIN
	SELECT JSON_OBJECT ('titre', video.titre, 'description', video.description)
    FROM video
    WHERE (video.idVideo = idVideo);
END


-- Question 2: newsletter avec toutes les sorties de la semaine
CREATE PROCEDURE `newsletter`()
BEGIN
    DECLARE compteur_videos NUMBER;
    DECLARE titre VARCHAR(255);
    DECLARE description VARCHAR(255);

    -- Toutes les vidéos qui sont sorties il y a au plus 7 jours
    DECLARE vid_cursor CURSOR FOR
        SELECT titre, description 
        FROM video 
        WHERE (DATEDIFF(CURRENT_DATE, video.date_sortie) <= 7);

    OPEN vid_cursor;
    compteur_videos := 0;
    DBMS_OUTPUT.put_line('Voici les sorties de la semaine');

    LOOP
        FETCH vid_cursor INTO titre, description;
        EXIT WHEN vid_cursor%NOTFOUND;

        compteur_videos := compteur_videos + 1;
        DBMS_OUTPUT.put_line(compteur_videos || ') ' || titre || ' - ' || description);
    END LOOP;
    CLOSE vid_cursor;
END

-- Question 3: suggestions des vidéos populaires en fonction des catégories suivies par un utilisateur
CREATE PROCEDURE `getVideosPopulaires`(categorie VARCHAR(255))
BEGIN
    DECLARE titre VARCHAR(255);

    -- Renvoie les 5 vidéos les plus populaires d'une catégorie donnée
    DECLARE categorie_cursor CURSOR FOR
        SELECT video.titre,
        COUNT(historique.idVideo) as nombreVisionnages
        FROM emission, historique, video
        WHERE (emission.categorie = categorie) and (emission.titre = video.emission) AND (video.idVideo = historique.idVideo)
        group by video.titre
        ORDER BY nombreVisionnages DESC
        LIMIT 5;
        
    OPEN categorie_cursor;

    LOOP
        FETCH categorie_cursor into titre;
        EXIT WHEN categorie_cursor%NOTFOUND;
        DBMS_OUTPUT.put_line(titre);
    END LOOP;
    
    CLOSE categorie_cursor;
END

CREATE OR REPLACE PROCEDURE `getSuggestions`(loginUser VARCHAR(255))
BEGIN
    DECLARE categorie VARCHAR(255);
	
	-- On récupère toutes les catégories que suit l'utilisateur
    DECLARE categorie_cursor CURSOR FOR
		SELECT emission.categorie FROM emission
        JOIN abonnements ON (abonnements.login = loginUser) AND (abonnements.titreEmission = emission.titre);
        
    OPEN categorie_cursor;

    LOOP
        FETCH categorie_cursor INTO categorie;
        EXIT WHEN categorie_cursor%NOTFOUND;   
        CALL getVideosPopulaires(categorie);
    END LOOP;

	CLOSE categorie_cursor;
END